export const LOGIN = "logIn";
export const AUTH_CHECK = "isUserAuthenticated";
export const LOGOUT = "logOut";
