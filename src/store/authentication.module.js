import Web from "../services/web";
import jwt from "../services/jwt";

import {
  LOGIN,
  AUTH_CHECK,
  LOGOUT
} from "./actions.type";
import {
  SET_AUTH,
  SET_ERROR,
  PURGE_AUTH
} from "./mutations.type";

const state = {
  user: {},
  isAuthenticated: !!jwt.getToken(),
  errors: null
};

const getters = {
  user(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  }
};

const actions = {
  [LOGIN](context, params) {
    return new Promise((resolve, reject) => {
      Web.post("login", params)
        .then(({
          data
        }) => {
          context.commit(SET_AUTH, data);
          jwt.saveToken(data["access_token"]);
          resolve(data);
        })
        .catch(({
          response
        }) => {
          context.commit(SET_ERROR, response.data.errors);
          reject(response.data.errors);
        });
    });
  },
  [AUTH_CHECK](context) {
    if (jwt.getToken()) {
      Web.setHeader();
      Web.get("user")
        .then(({
          data
        }) => {
          context.commit(SET_AUTH, data);
        })
        .catch(({
          response
        }) => {
          context.commit(PURGE_AUTH);
          context.commit(SET_ERROR, response.data.errors);
        });
    } else {
      context.commit(PURGE_AUTH);
    }
  },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH);
  }
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_AUTH](state, data) {
    state.isAuthenticated = true;
    state.user = data.user;
    state.errors = {};
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    jwt.destroyToken();
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};