import axios from "axios";
import Jwt from "./jwt";
import {
  API_URL
} from "../common/config";

const Web = {
  init() {
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common[
      "Content-Type"
    ] = `application/x-www-form-urlencoded`;
  },

  setHeader() {
    axios.defaults.headers.common["Authorization"] = `Bearer ${Jwt.getToken()}`;
  },

  query(resource, params) {
    return axios
      .get(resource, {
        params
      })
      .catch(error => {
        throw new Error(`[RWV] Web Service ${error}`);
      });
  },

  get(resource, slug = "", params = {}) {
    if (slug) slug = "/" + slug;
    return axios
      .get(`${resource}${slug}`, {
        params
      })
      .catch(error => {
        throw new Error(`[RWV] Web Service ${error}`);
      });
  },

  post(resource, params) {
    return axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return axios.put(`${resource}`, params);
  },

  delete(resource) {
    return axios.delete(resource).catch(error => {
      throw new Error(`[RWV] Web Service ${error}`);
    });
  }
};

export default Web;

export const BranchService = {
  query(params = {}) {
    return Web.query("branches", params);
  },
  edit(id) {
    return Web.get(`branches/${id}/edit`);
  },
  show(id) {
    return Web.get(`branches/${id}`);
  },
  get(id) {
    return Web.get("branches", id);
  },
  store(params) {
    return Web.post("branches", params);
  },
  update(id, params) {
    return Web.put(`branches/${id}`, params);
  },
  destroy(id) {
    return Web.delete(`branches/${id}`);
  },
  getDate(id, params) {
    return Web.get(`branches/${id}/getCheque`, "", params);
  }
};
export const BankService = {
  query(params = {}) {
    return Web.query("banks", params);
  },
  // getCheques(params) {
  //   return Web.get(`banks/search`, '', params);
  // },
  edit(id) {
    return Web.get(`banks/${id}/edit`);
  },
  show(id) {
    return Web.get(`banks/${id}`);
  },
  store(params) {
    return Web.post("banks", params);
  },
  update(id, params) {
    return Web.put(`banks/${id}`, params);
  },
  destroy(id) {
    return Web.delete(`banks/${id}`);
  }
};

export const PartyService = {
  query(params = {}) {
    return Web.query("parties", params);
  },
  edit(id) {
    return Web.get(`parties/${id}/edit`);
  },
  show(id) {
    return Web.get(`parties/${id}`);
  },
  get(id) {
    return Web.get("parties", id);
  },
  store(params) {
    return Web.post("parties", params);
  },
  update(id, params) {
    return Web.put(`parties/${id}`, params);
  },
  destroy(id) {
    return Web.delete(`parties/${id}`);
  },
  getDate(id, params) {
    return Web.get(`parties/${id}/getCheque`, "", params);
  }
};

export const ChequeService = {
  query(params = {}) {
    return Web.query("cheques", params);
  },
  edit(id) {
    return Web.get(`cheques/${id}/edit`);
  },
  withdraw(params) {
    window.console.log(params);
    return Web.post(`cheque/withdraw`, params);
  },
  bounce(id) {
    return Web.get(`cheque/${id}/bounce`);
  },
  show(id) {
    return Web.get(`cheques/${id}`);
  },
  transaction(params) {
    return Web.get(`cheques/transaction`, "", params);
  },
  get(id) {
    return Web.get("cheques", id);
  },
  store(params) {
    return Web.post("cheques", params);
  },
  update(id, params) {
    return Web.put(`cheques/${id}`, params);
  },
  destroy(id) {
    return Web.delete(`cheques/${id}`);
  },
  getCheques(params) {
    return Web.get(`cheques/search`, "", params);
  }
};