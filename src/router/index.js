import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
// import Moment from 'vue-moment';

Vue.use(VueRouter);
// Vue.use(Moment);

const routes = [{

    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/branches",
    name: "branches",
    component: () => import("../views/Branches.vue")
  },
  {
    path: "/branches/:id",
    name: "branch",
    component: () => import("../views/Branch.vue")
  },
  {
    path: "/banks/",
    name: "banks",
    component: () => import("../views/Banks.vue")
  },
  {
    path: "/banks/:id",
    name: "bank",
    component: () => import("../views/Bank.vue")
  },
  // {
  //   path: "/cheques/:id/export",
  //   name: "exportCheque",
  //   component: () => import("../views/PrintChequeReport.vue")
  // },

  {
    path: "/parties",
    name: "parties",
    component: () => import("../views/Parties.vue")
  },
  {
    path: "/parties/:id",
    name: "party",
    component: () => import("../views/Party.vue")
  },
  {
    path: "/cheques",
    name: "cheques",
    component: () => import("../views/Cheque.vue")
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;