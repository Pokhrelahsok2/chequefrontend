import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";

import { AUTH_CHECK } from "./store/actions.type";

import WebService from "./services/web";
Vue.config.productionTip = false;

WebService.init();
router.beforeEach((to, from, next) =>
  Promise.all([store.dispatch(AUTH_CHECK)]).then(next)
);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
